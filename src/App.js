import React, { Component } from 'react';
import './App.css';

import InfoBx from './InfoBx'

//pseudo rosnaca baza danych
const data = [
  {id: 1, title: 'Wiadomosc nr 1', body: "Zawartość wiadomości nr 1 ...", date: "3/10 08:02"},
]

const inboxSize = 3;

setInterval(() =>{
  const index = data[data.length-1].id+1;
  if(data.length < inboxSize){
  let date = new Date();
    data.push({
    id: index,
    title: `Wiadomość nr ${index}`,
    body: `Zawartość wiadomości nr ${index}`,
    date: `${date.getDay()}/${date.getMonth()}
        ${date.getHours() < 10 ? "0"+date.getHours():date.getHours()}:${date.getMinutes() < 10 ? "0"+date.getMinutes() : date.getMinutes()}`
    })
    console.log(data)
  }
}, 4000)

const InfoBox = (props) =>{
  return(
    <div className="infoBox">
      <p>{props.content}</p>
    </div>
  )
}


class App extends Component {
  state = {
    comments: [...data],
    newInfo: []
  }

  getData = () =>{
    const count = data.length-this.state.comments.length;
    if(this.state.comments.length !== data.length){
      this.setState({
      comments: [...data]
    })
    }
    if(data.length === inboxSize){
      this.setState({
        newInfo: "inbox is full"
      })
  }else{
    this.setState({
      newInfo: `fetched ${count} messeges`
    })
  }
  }

  
  componentDidMount() { 
    this.setState({
      newInfo: `fetching data...`
    })
    this.idI = setInterval(this.getData, 2000)
  }

  componentWillUnmount(){
    clearInterval(this.idI)
  }

  render(){
    const comments = this.state.comments.map(c => (
      <div className="messageItem" key={c.id}>
          <h4>{c.title} <em>{c.body}</em></h4>
          <div>
          <p>{c.date}</p>
          </div>
        </div>
    ))

    return(
      <div>
        {/* <InfoBox content={this.state.newInfo}/> */}
        <InfoBx content={this.state.newInfo}/>
      <div className="messageBox">
        {comments.reverse()}
      </div>
      </div>
    )

}
}

export default App;
