import React, { Component } from 'react';
import './InfoBx.css';

class InfoBx extends Component {
    state = { 
        messages: []
     }

     componentDidMount(){
        this.cleanerI = setInterval(this.cleaner, 500)
     }

     componentWillUnmount(){
        clearInterval(this.cleanerI)
     }

     cleaner = () =>{
        let msg = [...this.state.messages] 
            const numberMessagesToDelete = (-3+msg.length)
            msg.splice(0, numberMessagesToDelete)
            this.setState({
                messages: msg
            })
     }

     componentDidUpdate(){
         const msg = [...this.state.messages]
         if(msg[msg.length-1] !== this.props.content){
            msg.push(this.props.content)
              this.setState({
                 messages: msg
             })
         }
     }

    render() { 
        const messeges = this.state.messages.map((msg, i) => (
            <p key={i}>{msg}</p>
        ))
        return ( 
            <div className="infoBox">
            {messeges}
          </div>
         );
    }
}
 
export default InfoBx;